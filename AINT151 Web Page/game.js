var storyOutputArray = [
  "Hey Mason, it’s Alex, you ready? Let’s run through the brief again. We are trying to infiltrate David Trust’s Vault. The safe plan is to disguise yourself as a banker, enter his vault and come back to me in the getaway car with as much money as you can carry. Stealth is the key. All you have is a very strong sleeping pill. You have to do it quickly as they’ll be suspicious of me in a car waiting outside the main entrance for too long, I’ll stay 5 minutes and I’m out of there. Once you leave this car, time will be ticking. I’ll warn you about time through our ear pieces.",
  "You notice there is a keypad by the door meaning that you’ll need a code to enter this way.",
  "As you walk in, you see the room is littered with workers, guards and security cameras. You will struggle to do anything secretive in this room. You will also see a door that requires a code to open. You’re immediately approached by one of the bankers asking if you need his assistance.",
  "The Banker leads you into his office. He pours two drinks and continues the discussion. Eventually, he goes towards a file cabinet to retrieve some files.",
  "The Banker passes out after a sip of water. You instantly take his clothes and disguise yourself as him and hide the body the best you can. You rummage through the pockets and find nothing.",
  "You notice an air vent but it’s out of reach. There is nothing else out in the open noteworthy. Maybe the pin code is hidden somewhere.",
  "At the back of a cluttered draw is a worn out sticky note with the two 4 digit codes “7206” and “3854”.",
  "As you walk towards the locked door, no one suspects a thing, as everyone working at the bank is doing their own job and you don’t stand out. You get to the door and see the keypad. Use the radio buttons to enter the pin code.",
  "The door opens and you enter. In the staff room is a Banker, fortunately, he doesn’t recognise you and doesn’t question why you’re in the staff room as you’re wearing the appropriate attire. There is a lonely bag on the floor which you pick up.",
  "You enter a corridor which leads to all the vaults the bank contains. There is a guard patrolling the area who sees you but presumes you work there. At Trust’s vault is another keypad.",
  "You’re in! You start throwing money into the bag until its full.",
  "You run towards the bathroom and search the room. You see an air vent leading out of the room.",
  "You climb up the bathroom stall closest to the air vent. You can reach it! You open a grill panel and leap into the vents. Now you have to decide where to drop down from.",
  "You drop down into the staff bathroom and you actually land on a cleaner, taking him out in the process. You take his overalls and cleaning cart and disguise yourself as the cleaner.",
  "You enter the staff room and there is a banker in there who seems to be on a break. He takes a quick glance at you and sees your overalls and goes back to his phone.",
  "You enter a corridor that leads to all the vaults. A guard is patrolling the corridor; he doesn’t suspect anything.",
  "In the cleaner’s trolley there is a mini water dispenser. You offer the guard some water which he gladly accepts. You pour a drink and discretely add the sleeping pill to the drink. You hand over the drink and after a sip, the guard passes out.",
  "At Trust’s Vault Door, you see it requires either a pin code or a key.",
  "You check the pockets and there’s nothing. However, there’s a ring of keys attached to his belt which you grab.",
  "You now have a way to enter the vault but only 1 of the 10 keys on the ring will work. Time for some trial and error.",
  "That was the right key! You’re in! You start throwing money into the cleaner’s bin in the cart until it’s full.",
  "You’ve got into the car without looking suspicious. You’ve done it! You successfully managed to retrieve £100,000 from Trust’s Vault!",
  "You’ve got into the car without looking suspicious. You’ve done it! You successfully managed to retrieve £1,000,000 from Trust’s Vault!",
  "The receptionist doesn’t recognise you and asks for the guard. The guard asks for Bank ID which you do not have and you’re arrested on the spot.",
  "The co-worker doesn’t recognise you and asks for the guard. The guard asks for Bank ID which you do not have and you’re arrested on the spot.",
  "The alarm is set off, you try to run out of the bank but run into multiple guards and you are arrested",
  "You drop down into the Main Foyer, multiple guards swarm onto you and arrest you.",
  "You drop down into the Office where a Banker is working. The banker sounds the alarm and guards rush towards your location and arrest you.",
  "You drop down into the Staff Room where a Banker is having a break. He instantly shouts that there’s an intruder. Guards rush in and arrest you.",
  "You drop into the Corridor where a guard is patrolling the area. He tackles you as soon as you hit the ground and arrests you.",
  "The guard tackles you to the floor and arrests you as the cleaner is on strict instructions not to be near the vaults.",
  "You’ve spent too long in the Bank and Alex the getaway driver has had to drive away."
]

var ButtonText1Array = [
  "Retry",
  "Enter through the Main Entrance",
  "“Yes please, I’d like to access my Vault”",
  "Add the sleeping pill to his drink",
  "Ask receptionist for pin code",
  "Search Filing Cabinet",
  "Head towards the locked door in the Main Foyer",
  "Head towards the Vault",
  "Walk back towards the car",
  "Attempted to get into the air vent",
  "Main Foyer",
  "Enter Staff Room",
  "Head towards the Vault",
  "Head straight towards Trust’s Vault",
  "Head to the vault",
  "Search the guard",
  "Head towards Trust’s Vault",
  "Head back to the car"
]

var ButtonText2Array = [
  "Go towards the Staff Entrance",
  "“No sorry, do you have a bathroom I can use please!”",
  "Ask co-worker for pin code",
  "Search Picture Frame",
  "Office",
  "Offer guard a drink with sleeping pill",
  "Search the guard"
]

var ButtonText3Array = [
  "Search the office",
  "Search Desk Draw",
  "Staff Room"
]

var index = 0;

function OnLoad() {
  document.getElementById("ButtonText3").style.visibility = "hidden";
  document.getElementById("ButtonText4").style.visibility = "hidden";
  document.getElementById("ButtonText5").style.visibility = "hidden";
  document.getElementById("keypad").style.visibility = "hidden";
  document.getElementById("keySelect").style.visibility = "hidden";
  document.getElementById("keyButton").style.visibility = "hidden";
}

function Button1StoryProgress() {
  if (index == 0) {
    document.getElementById("ButtonText1").innerHTML = ButtonText1Array[2];
    document.getElementById("ButtonText2").innerHTML = ButtonText2Array[1];
    document.getElementById("storyOutput").innerHTML = storyOutputArray[2];
    index = 2;
    document.getElementById("ButtonText2").style.bottom = "338px";
    document.getElementById("ButtonText2").style.padding = "30px 10px 30px 10px";
    var userTimer = 60 * 3,
          display = document.getElementById("time");
      startTimer(userTimer, display);
      document.getElementById("time").style.visibility = "visible";
  }
  else if (index == 1) {
    document.getElementById("ButtonText1").innerHTML = ButtonText1Array[2];
    document.getElementById("ButtonText2").style.visibility = "visible";
    document.getElementById("storyOutput").innerHTML = storyOutputArray[2];
    document.getElementById("ButtonText2").innerHTML = ButtonText2Array[1];
    index = 2;
    document.getElementById("ButtonText2").style.bottom = "338px";
    document.getElementById("ButtonText2").style.padding = "30px 10px 30px 10px";
  }
  else if (index == 2) {
    document.getElementById("ButtonText1").innerHTML = ButtonText1Array[3];
    document.getElementById("storyOutput").innerHTML = storyOutputArray[3];
    document.getElementById("ButtonText2").style.visibility = "hidden";
    index = 3;
  }
  else if (index == 3) {
    document.getElementById("ButtonText1").innerHTML = ButtonText1Array[4];
    document.getElementById("ButtonText2").innerHTML = ButtonText2Array[2];
    document.getElementById("ButtonText3").innerHTML = ButtonText3Array[0];
    document.getElementById("storyOutput").innerHTML = storyOutputArray[4];
    document.getElementById("ButtonText2").style.visibility = "visible";
    document.getElementById("ButtonText3").style.visibility = "visible";
    document.getElementById("ButtonText2").style.bottom = "335px";
    document.getElementById("ButtonText2").style.padding = "40px 10px 40px 10px";
    index = 4;
  }
  else if (index == 4) {
    document.getElementById("ButtonText1").innerHTML = ButtonText1Array[0];
    document.getElementById("storyOutput").innerHTML = storyOutputArray[23];
    document.getElementById("ButtonText2").style.visibility = "hidden";
    document.getElementById("ButtonText3").style.visibility = "hidden";
    index = 21;
  }
  else if (index == 5) {
    document.getElementById("storyOutput").innerHTML = "Nothing. Try Again.";
    document.getElementById("ButtonText1").style.textDecoration = "line-through";
  }
  else if (index == 6) {
    document.getElementById("ButtonText1").innerHTML = ButtonText1Array[7];
    document.getElementById("storyOutput").innerHTML = storyOutputArray[7];
    document.getElementById("ButtonText1").style.padding = "40px 10px 40px 10px";
    document.getElementById("ButtonText1").style.visibility = "hidden";
    index = 7;
    document.getElementById("keypad").style.visibility = "visible";
  }
  else if (index == 7) {
    document.getElementById("ButtonText1").innerHTML = ButtonText1Array[8];
    document.getElementById("storyOutput").innerHTML = storyOutputArray[9];
    document.getElementById("ButtonText1").style.visibility = "hidden";
    document.getElementById("keypad").style.visibility = "visible";
    index = 8;
  }
  else if (index == 8) {
    document.getElementById("ButtonText1").innerHTML = ButtonText1Array[0];
    document.getElementById("storyOutput").innerHTML = storyOutputArray[21];
    index = 21;
  }
  else if (index == 9) {
    document.getElementById("ButtonText1").innerHTML = ButtonText1Array[10];
    document.getElementById("ButtonText2").innerHTML = ButtonText2Array[4];
    document.getElementById("ButtonText3").innerHTML = ButtonText3Array[2];
    document.getElementById("ButtonText4").innerHTML = "Corridor";
    document.getElementById("ButtonText5").innerHTML = "Staff Bathroom";
    document.getElementById("storyOutput").innerHTML = storyOutputArray[12];
    document.getElementById("ButtonText2").style.visibility = "visible";
    document.getElementById("ButtonText3").style.visibility = "visible";
    document.getElementById("ButtonText4").style.visibility = "visible";
    document.getElementById("ButtonText5").style.visibility = "visible";
    index = 10;
  }
  else if (index == 10) {
    document.getElementById("ButtonText1").innerHTML = ButtonText1Array[0];
    document.getElementById("storyOutput").innerHTML = storyOutputArray[26];
    document.getElementById("ButtonText2").style.visibility = "hidden";
    document.getElementById("ButtonText3").style.visibility = "hidden";
    document.getElementById("ButtonText4").style.visibility = "hidden";
    document.getElementById("ButtonText5").style.visibility = "hidden";
    index = 21;
  }
  else if (index == 11) {
    document.getElementById("ButtonText1").innerHTML = ButtonText1Array[12];
    document.getElementById("storyOutput").innerHTML = storyOutputArray[14];
    index = 12;
  }
  else if (index == 12) {
    document.getElementById("ButtonText1").innerHTML = ButtonText1Array[13];
    document.getElementById("ButtonText2").innerHTML = ButtonText2Array[5];
    document.getElementById("storyOutput").innerHTML = storyOutputArray[15];
    document.getElementById("ButtonText2").style.visibility = "visible";
    index = 13;
  }
  else if (index == 13) {
    document.getElementById("ButtonText1").innerHTML = ButtonText1Array[0];
    document.getElementById("storyOutput").innerHTML = storyOutputArray[30];
    document.getElementById("ButtonText2").style.visibility = "hidden";
    index = 21;
  }
  else if (index == 14) {
    document.getElementById("ButtonText1").innerHTML = ButtonText1Array[15];
    document.getElementById("storyOutput").innerHTML = storyOutputArray[17];
    document.getElementById("ButtonText2").style.visibility = "hidden";
    index = 15;
  }
  else if (index == 15) {
    document.getElementById("ButtonText1").innerHTML = ButtonText1Array[16];
    document.getElementById("storyOutput").innerHTML = storyOutputArray[18];
    index = 16;
  }
  else if (index == 16) {
    document.getElementById("ButtonText1").innerHTML = ButtonText1Array[17];
    document.getElementById("storyOutput").innerHTML = storyOutputArray[19];
    document.getElementById("ButtonText1").style.visibility = "hidden";
    document.getElementById("keySelect").style.visibility = "visible";
    document.getElementById("keyButton").style.visibility = "visible";
    index = 17;
  }
  else if (index == 20) {
    document.getElementById("ButtonText1").innerHTML = ButtonText1Array[0];
    document.getElementById("storyOutput").innerHTML = storyOutputArray[22];
    index = 21;
  }

  else if (index == 21) {
    document.getElementById("ButtonText1").innerHTML = ButtonText1Array[1];
    document.getElementById("storyOutput").innerHTML = storyOutputArray[0];
    document.getElementById("ButtonText2").style.visibility = "visible";
    document.getElementById("ButtonText2").innerHTML = ButtonText2Array[0];
    document.getElementById("time").innerHTML = "";
    document.getElementById("time").style.visibility = "hidden";
    index = 0;
  }
}

function Button2StoryProgress() {
  if (index == 0) {
    document.getElementById("ButtonText1").innerHTML = ButtonText1Array[1];
    document.getElementById("ButtonText2").style.visibility = "hidden";
    document.getElementById("storyOutput").innerHTML = storyOutputArray[1];
    var userTimer = 60 * 3,
          display = document.getElementById("time");
      startTimer(userTimer, display);
      document.getElementById("time").style.visibility = "visible";
    index = 1;
  }
  else if (index == 2) {
    document.getElementById("ButtonText1").innerHTML = ButtonText1Array[9];
    document.getElementById("storyOutput").innerHTML = storyOutputArray[11];
    document.getElementById("ButtonText2").style.visibility = "hidden";
    document.getElementById("ButtonText2").style.bottom = "335px";
    document.getElementById("ButtonText2").style.padding = "40px 10px 40px 10px";
    index = 9;
  }
  else if (index == 4) {
    document.getElementById("ButtonText1").innerHTML = ButtonText1Array[0];
    document.getElementById("storyOutput").innerHTML = storyOutputArray[24];
    document.getElementById("ButtonText2").style.visibility = "hidden";
    document.getElementById("ButtonText3").style.visibility = "hidden";
    index = 21;
  }
  else if (index == 5) {
    document.getElementById("storyOutput").innerHTML = "Nothing. Try Again.";
    document.getElementById("ButtonText2").style.textDecoration = "line-through";
  }
  else if (index == 10) {
    document.getElementById("ButtonText1").innerHTML = ButtonText1Array[0];
    document.getElementById("storyOutput").innerHTML = storyOutputArray[27];
    document.getElementById("ButtonText2").style.visibility = "hidden";
    document.getElementById("ButtonText3").style.visibility = "hidden";
    document.getElementById("ButtonText4").style.visibility = "hidden";
    document.getElementById("ButtonText5").style.visibility = "hidden";
    index = 21;
  }
  else if (index == 13) {
    document.getElementById("ButtonText1").innerHTML = ButtonText1Array[14];
    document.getElementById("ButtonText2").innerHTML = ButtonText2Array[6];
    document.getElementById("storyOutput").innerHTML = storyOutputArray[16];
    index = 14;
  }
  else if (index == 14) {
    document.getElementById("ButtonText1").innerHTML = ButtonText1Array[16];
    document.getElementById("storyOutput").innerHTML = storyOutputArray[18];
    document.getElementById("ButtonText2").style.visibility = "hidden";
    index = 16;
  }
}

function Button3StoryProgress() {
  if (index == 4) {
    document.getElementById("ButtonText1").innerHTML = ButtonText1Array[5];
    document.getElementById("ButtonText2").innerHTML = ButtonText2Array[3];
    document.getElementById("ButtonText3").innerHTML = ButtonText3Array[1];
    document.getElementById("storyOutput").innerHTML = storyOutputArray[5];
    index = 5;
  }
  else if (index == 5) {
    document.getElementById("ButtonText1").innerHTML = ButtonText1Array[6];
    document.getElementById("ButtonText2").style.visibility = "hidden";
    document.getElementById("ButtonText3").style.visibility = "hidden";
    document.getElementById("storyOutput").innerHTML = storyOutputArray[6];
    document.getElementById("ButtonText1").style.padding = "30px 10px 30px 10px";
    document.getElementById("ButtonText1").style.textDecoration = "none";
    document.getElementById("ButtonText2").style.textDecoration = "none";
    index = 6;
  }
  else if (index == 10) {
    document.getElementById("ButtonText1").innerHTML = ButtonText1Array[0];
    document.getElementById("storyOutput").innerHTML = storyOutputArray[28];
    document.getElementById("ButtonText2").style.visibility = "hidden";
    document.getElementById("ButtonText3").style.visibility = "hidden";
    document.getElementById("ButtonText4").style.visibility = "hidden";
    document.getElementById("ButtonText5").style.visibility = "hidden";
    index = 21;
  }
}

function Button4StoryProgress() {
  if (index == 10) {
    document.getElementById("ButtonText1").innerHTML = ButtonText1Array[0];
    document.getElementById("storyOutput").innerHTML = storyOutputArray[29];
    document.getElementById("ButtonText2").style.visibility = "hidden";
    document.getElementById("ButtonText3").style.visibility = "hidden";
    document.getElementById("ButtonText4").style.visibility = "hidden";
    document.getElementById("ButtonText5").style.visibility = "hidden";
    index = 21;
  }
}

function Button5StoryProgress() {
  if (index == 10) {
    document.getElementById("ButtonText1").innerHTML = ButtonText1Array[11];
    document.getElementById("storyOutput").innerHTML = storyOutputArray[13];
    document.getElementById("ButtonText2").style.visibility = "hidden";
    document.getElementById("ButtonText3").style.visibility = "hidden";
    document.getElementById("ButtonText4").style.visibility = "hidden";
    document.getElementById("ButtonText5").style.visibility = "hidden";
    index = 11;
  }
}

function keypadStory() {
  document.getElementById("keypad").style.visibility = "hidden";
  if (document.getElementById("checkboxTwo").checked == true &&
       document.getElementById("checkboxSix").checked == true &&
       document.getElementById("checkboxSeven").checked == true &&
       document.getElementById("checkboxZero").checked == true &&
       document.getElementById("checkboxOne").checked == false &&
        document.getElementById("checkboxThree").checked == false &&
        document.getElementById("checkboxFour").checked == false &&
        document.getElementById("checkboxFive").checked == false &&
        document.getElementById("checkboxEight").checked == false &&
        document.getElementById("checkboxNine").checked == false &&
        index == 7){
    document.getElementById("ButtonText1").style.visibility = "visible";
    document.getElementById("storyOutput").innerHTML = storyOutputArray[8];
  }
  else if (document.getElementById("checkboxEight").checked == true &&
       document.getElementById("checkboxThree").checked == true &&
       document.getElementById("checkboxFive").checked == true &&
       document.getElementById("checkboxFour").checked == true &&
       document.getElementById("checkboxOne").checked == false &&
        document.getElementById("checkboxZero").checked == false &&
        document.getElementById("checkboxTwo").checked == false &&
        document.getElementById("checkboxSix").checked == false &&
        document.getElementById("checkboxSeven").checked == false &&
        document.getElementById("checkboxNine").checked == false &&
        index == 8) {
          document.getElementById("ButtonText1").style.visibility = "visible";
          document.getElementById("storyOutput").innerHTML = storyOutputArray[10];
  }
  else {
    document.getElementById("ButtonText1").innerHTML = ButtonText1Array[0];
    document.getElementById("storyOutput").innerHTML = storyOutputArray[25];
    document.getElementById("ButtonText1").style.visibility = "visible";
    index = 21;
  }
  document.getElementById("checkboxEight").checked = false;
  document.getElementById("checkboxThree").checked = false;
  document.getElementById("checkboxFive").checked = false;
  document.getElementById("checkboxFour").checked = false;
  document.getElementById("checkboxOne").checked = false;
  document.getElementById("checkboxZero").checked = false;
  document.getElementById("checkboxTwo").checked = false;
  document.getElementById("checkboxSix").checked = false;
  document.getElementById("checkboxSeven").checked = false;
  document.getElementById("checkboxNine").checked = false;
}

function keySelectStory() {
  var keySelection = document.getElementById("keySelect");
  var selectedKey = keySelection.options[keySelection.selectedIndex].value;
  if (selectedKey == "Rusty") {
    document.getElementById("storyOutput").innerHTML = storyOutputArray[20];
    document.getElementById("ButtonText1").style.visibility = "visible";
    document.getElementById("keySelect").style.visibility = "hidden";
    document.getElementById("keyButton").style.visibility = "hidden";
  }
  else {
    document.getElementById("storyOutput").innerHTML = "Key doesn't fit. Try again!";
  }
  index = 20;
}

function startTimer(duration, display) {
    var timer = duration, minutes, seconds;
    setInterval(function () {

        if (timer == 0) {
            index = 21;
            document.getElementById("time").innerHTML = "00:00";
            document.getElementById("ButtonText1").innerHTML = ButtonText1Array[0];
            document.getElementById("storyOutput").innerHTML = storyOutputArray[31];
            document.getElementById("ButtonText1").style.visibility = "visible";
            document.getElementById("ButtonText2").style.visibility = "hidden";
            document.getElementById("ButtonText3").style.visibility = "hidden";
            document.getElementById("ButtonText4").style.visibility = "hidden";
            document.getElementById("ButtonText5").style.visibility = "hidden";
            document.getElementById("keypad").style.visibility = "hidden";
            document.getElementById("keySelect").style.visibility = "hidden";
            document.getElementById("keyButton").style.visibility = "hidden";
        }

        if (index == 21) {
            timer = -1
                }

        else if (minutes == 5) {
          alert("Hi");
        }

        else if (timer > 0) {
          minutes = parseInt(timer / 60, 10);
          seconds = parseInt(timer % 60, 10);

          minutes = minutes < 10 ? "0" + minutes : minutes;
          seconds = seconds < 10 ? "0" + seconds : seconds;

          display.textContent = minutes + ":" + seconds;

          timer--;

        }
    }, 1000);
}
