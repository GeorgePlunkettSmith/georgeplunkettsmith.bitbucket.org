var weaponsArray = [
	'Broadsword',
	'Crossbow',
	'Magic staff'
];


function OnLoad()
{
  for(var i = 0; i < weaponsArray.length; i++)
  {
    var tag = "<option value='" + i + "'>" + weaponsArray[i] + "</option>";
    document.forms[0]["weapons"].innerHTML += tag;
  }
  document.getElementById("currentWeapon").innerHTML = weaponsArray[0];
}

function SelectWeapon()
{
  var currentWeaponIndex = document.forms[0]["weapons"].options[document.forms[0]["weapons"].selectedIndex].value;
  document.getElementById("currentWeapon").innerHTML = weaponsArray[currentWeaponIndex];
}
