
function StorePlayerData()
{
	var playerName = document.forms[0]["playername"].value;
  localStorage.setItem("playerName", playerName);

  var playerHealth = document.forms[0]["playerhealth"].value;
  localStorage.setItem("playerHealth", playerHealth);

  var playerColour = document.forms[0]["playercolour"].value;
  localStorage.setItem("playerColour", playerColour);
}
